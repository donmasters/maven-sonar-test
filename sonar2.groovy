pipeline {
  agent any

  stages {
    stage('Build') {
      steps {
        echo 'Building.. V4'
      }
    }
    stage("build & SonarQube analysis") {
      when {
        expression {
          return (params.parm_one == "apple" && params.parm_two == 'orange')
        }
      }
    agent any
      steps {
        withSonarQubeEnv('localsonar') {
          sh 'mvn clean package sonar:sonar -f my-app/pom.xml -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.jdbc.url="jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8&rewriteBatchedStatements=true" -Dsonar.jdbc.username=$SONAR_JDBC_USERNAME -Dsonar.jdbc.password=$SONAR_JDBC_PASSWORD'
        }
//          sh 'mvn -f my-app/pom.xml -U -C verify'
      }
    }
    stage('Test') {
      steps {
        echo 'Testing..'
      }
    }
    stage('Deploy') {
      steps {
        echo 'Deploying....'
      }
    }
  }
}
